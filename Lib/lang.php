<?php
/**
 * COmanage Registry KDC Enroller Plugin Language File
 *
 */
  
global $cm_lang, $cm_texts;

// When localizing, the number in format specifications (eg: %1$s) indicates the argument
// position as passed to _txt.  This can be used to process the arguments in
// a different order than they were passed.
$cm_kdc_enroller_texts['en_US'] = array(
	// Titles, per-controll
 //'ct.co_enrollment_flow_wedges.1'  => 'Enrollment Flow Wedge',
 // 'ct.co_enrollment_flow_wedges.pl' => 'Enrollment Flow Wedges',
  'in.pl.noconfig'  => 'There is no need for configuration'

);

