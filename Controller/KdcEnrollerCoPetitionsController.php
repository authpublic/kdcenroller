<?php

App::uses('CoPetitionsController', 'Controller');

class KdcEnrollerCoPetitionsController extends CoPetitionsController {
  // Class name, used by Cake
  public $name = "KdcEnrollerCoPetitions";

  // Add other classes you might need to use here.
  public $uses = array(
                  'CoPetition',
                 );

  // See this diagram 
  // https://spaces.at.internet2.edu/display/COmanage/Registry+Enrollment+Flow+Diagram
  // for the enrollment flow.
  // We provide a plugin for the finalize step. At this point the petition
  // is approved and identifiers, including the Kerberos Principal, have already
  // been assigned. 
  protected function execute_plugin_finalize($id, $onFinish) {

    // Use the petition id to find the petition. The EnrolleeCoPerson model
    // is just an alias for the CoPerson model. Using a contain we can pull
    // the EnrolleeCoPerson, CoOrgIdentityLink, OrgIdentity, and Identifier
    // models all at once.
    $args = array();
    $args['conditions']['CoPetition.id'] = $id;
    $args['contain'] = array();
    $args['contain']['EnrolleeCoPerson']['CoOrgIdentityLink']['OrgIdentity'] = array();
    $args['contain']['EnrolleeCoPerson']['Identifier'] = array();

    $coPetition = $this->CoPetition->find('first', $args);
    if (empty($coPetition)) {
      $this->Flash->set(_txt("Could not find petition with ID $id"), array('key' => 'error'));
      $this->redirect("/");
      return;
    }
    $this->log("CoPetition Data======" . print_r($coPetition, true));

    // We assume that there is only one OrgIdentity.
#    $orgId = $coPetition['OrgIdentity'][0]['id'];
        $orgId = $coPetition['EnrolleeCoPerson']['CoOrgIdentityLink'][0]['OrgIdentity']['id'];

     $this->log("org id=============" . print_r($orgId, true));

    // Loop over the identifiers attached to the CoPerson record
    // to find the Kerberos Principal.
    $kerberosPrincipal = "";
   # foreach($coPetition['Identifier'] as $identifier) {
	 foreach($coPetition['EnrolleeCoPerson']['Identifier'] as $identifier) {

      if($identifier['type'] == 'kerberosprincipal' && $identifier['status'] == SuspendableStatusEnum::Active) {
        $kerberosPrincipal = $identifier['identifier'];
        break;
      }
    }

    // Now create a new Identifier and attach it to the Org Identity.
    // We mark it as a login identifier so that it can be used to 
    // login to COmanage Registry.
    $data = array();
    $data['Identifier']['identifier'] = $kerberosPrincipal . "@ligo-india.in";
    $data['Identifier']['type'] = 'eppn';
    $data['Identifier']['status'] =  SuspendableStatusEnum::Active;
    $data['Identifier']['org_identity_id'] = $orgId;
    $data['Identifier']['login'] = true;

    // Always clear a model before saving to make sure there is no 
    // bad data in the model.
    $this->CoPetition->EnrolleeCoPerson->CoOrgIdentityLink->OrgIdentity->Identifier->clear();

    if(!$this->CoPetition->EnrolleeCoPerson->CoOrgIdentityLink->OrgIdentity->Identifier->save($data)) {
      $this->Flash->set(_txt("Could not save Identifier for $kerberosPrincipal"), array('key' => 'error'));
      $this->redirect("/");
      return;

    }

    // We are done so redirect the browser using the URL we were given.
    $this->redirect($onFinish);
  }
}
